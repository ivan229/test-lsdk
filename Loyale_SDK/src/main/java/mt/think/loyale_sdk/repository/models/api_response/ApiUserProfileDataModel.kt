package mt.think.loyale_sdk.repository.models.api_response

import mt.think.loyale_sdk.repository.models.api_inner_models.ApiProfileAdditionalField

data class ApiUserProfileDataModel(
    val additionalFields: ArrayList<ApiProfileAdditionalField>,
    val customer: ApiProfileCustomer
)