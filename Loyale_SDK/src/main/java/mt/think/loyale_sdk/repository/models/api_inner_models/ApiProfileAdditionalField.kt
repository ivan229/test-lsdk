package mt.think.loyale_sdk.repository.models.api_inner_models

data class ApiProfileAdditionalField(
//        val createdDate: String,
    val customerId: String,
    val id: String,
    val key: String,
    val name: String,
    val schemeId: String,
//        val updatedDate: String,
    val value: String?
)