package mt.think.loyale_sdk.repository.models.api_body

data class ApiRequestChangePassDataModel(
    val id: String,
    val currentPassword: String,
    val newPassword: String,
    val confirmNewPassword: String
)