package mt.think.loyale_sdk.repository.models.api_inner_models

data class ApiRegistrationAdditionalField(
    val key: String,
    val name: String,
    val value: String?
)