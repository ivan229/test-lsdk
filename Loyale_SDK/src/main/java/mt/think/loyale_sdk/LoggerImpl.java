package mt.think.loyale_sdk;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

class LoggerImpl {
    private final String TAG = "MyLogger";

    public void loge(@NonNull String tagOrMsg, @Nullable String... msgs) {
        if (msgs == null) {
            Log.e(TAG, tagOrMsg);
        } else if (msgs != null) {
            StringBuilder messageBuilder = new StringBuilder("");
            for (int i = 0; i < msgs.length; i++) {
                messageBuilder.append(msgs[i]);
                if (msgs.length - 1 > i)
                    messageBuilder.append(" - ");
            }
            Log.d(tagOrMsg, messageBuilder.toString());
        }
    }
}
