package mt.think.loyale_sdk.repository.network

import mt.think.loyale_sdk.repository.models.api_body.*
import mt.think.loyale_sdk.repository.models.api_response.*
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.Response
import retrofit2.http.*

interface ApiService {
    @GET("api/helpers/mobilecode")
    suspend fun getAreasCodes(): Response<ArrayList<ApiAreaCodesDataModel>>

    @GET("/api/Helpers/Countries")
    suspend fun getCountriesList(): Response<ArrayList<ApiCountryModel>>

    @GET("api/Helpers/Towns")
    suspend fun getTownsByLocale(
        @Header("Scheme") schemeId: String,
        @Query("countryCode") locale: String
    ): Response<ArrayList<ApiTown>>

    @Multipart
    @POST("api/Upload/ProfilePicture")
    suspend fun uploadProfilePicture(
        @Header("Scheme") schemeId: String,
        @Header("Authorization") authHeader: String,
        @Query("customerId") customerId: String,
        @Part("description") description: RequestBody,
        @Part file: MultipartBody.Part
    ): Response<Any>

    @GET("api/Customer/{id}")
    suspend fun getCustomer(
        @Header("Scheme") schemeId: String,
        @Path("id") customerId: String
    ): Response<ApiCustomerResultView>

//    /**
//     * user auth
//     */
//    @POST("api/token")
//    suspend fun authUser(
//        @Header("Scheme") schemeId: String,
//        @Body loginToPasswordModel: LoginRequestModel
//    ): Response<LoginResponseDataModel>
//
//    @POST("api/PushToken")
//    suspend fun registerPushToken(
//        @Header("Scheme") schemeId: String,
//        @Body pushTokenModel: ApiRequestRegistedPushToken
//    ): Response<Any>
//
//    @DELETE("/api/PushToken")
//    suspend fun unsubscribeNotifications(
//        @Header("Scheme") schemeId: String,
//        @Header("Authorization") authHeader: String,
//        @Query("token") notificationToken: String
//    ): Response<Any>
//
//    @GET("api/Customer/GetAdditional")
//    suspend fun getCustomerData(
//        @Header("Authorization") authHeader: String,
//        @Header("Scheme") schemeId: String,
//        @Query("customerId") customerId: String
//    ): Response<ApiUserProfileDataModel>
//
//    // API CUSTOMER MISSING ======================================================================================
//
//    @GET("api/Franchise")
//    suspend fun getFranchise(
//        @Header("Scheme") schemeId: String,
//        @Header("Authorization") authHeader: String,
//        @Query("Filters") filter: String?
//    ): Response<ArrayList<ApiResponseFranchiseDataModel>>
//
//    @POST("api/customer/PostAdditional")
//    suspend fun registerNewUser(
//        @Header("Scheme") schemeId: String,
//        @Body profileModel: ApiRegistrationDataModel
//    ): Response<Any>
//
//    /**
//     * request to send email with link to restore pass to specified email
//     * in case of succeed expect some string like "Email for resetting password was sent to: email2@new.com"
//     */
//    @POST("api/Account/ForgotPassword")
//    suspend fun requestRestoringPass(
//        @Header("Scheme") schemeId: String,
//        @Body email: ApiRequestEmailDataModel
//    ): Response<String>
//
//    // API COUPONS LINKED MISSING ======================================================================================
//
//    /**
//     * getting level by ID
//     */
//    @GET("/api/Level/{id}")
//    suspend fun getLevelById(
//        @Header("Authorization") authHeader: String,
//        @Header("Scheme") schemeId: String,
//        @Path("id") levelId: String
//    ): Response<ApiLevelDataModel>
//
//    @POST("/api/Account/ChangePassword")
//    suspend fun changePassword(
//        @Header("Scheme") schemeId: String,
//        @Header("Authorization") authHeader: String,
//        @Body changePassData: ApiRequestChangePassDataModel
//    ): Response<Any>
//
//    @Multipart
//    @POST("/api/Upload")
//    suspend fun uploadPhoto(
//        @Header("Scheme") schemeId: String,
//        @Header("Authorization") authHeader: String,
//        @Part("description") description: RequestBody,
//        @Part file: MultipartBody.Part
//    ): Response<ApiResponsePhotoUpload>
//
//    @POST("/api/Contact/match")
//    suspend fun matchContacts(
//        @Header("Scheme") schemeId: String,
//        @Header("Authorization") authHeader: String,
//        @Body contacts: ApiRequestMatchContactsDataModel
//    ): Response<ApiResponseMatchContactsDataModel>
//
//    @POST("/api/Message")
//    suspend fun sendMessage(
//        @Header("Scheme") schemeId: String,
//        @Header("Authorization") authHeader: String,
//        @Body message: ApiRequestMessage
//    ): Response<Any>
//
//    @GET("/api/CurrencyPointConversionOverride")
//    suspend fun getCurrencyPointConversionOverride(
//        @Header("Scheme") schemeId: String,
//        @Header("Authorization") authHeader: String,
//        @Query("Filters") filter: String?
//    ): Response<ArrayList<ApiResponseCoversionRate>>
}