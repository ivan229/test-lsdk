package mt.think.loyale_sdk.repository.models

import com.google.gson.Gson

//custom response errors
// unexpected error
const val UNEXPECTED_ERROR_CODE = 0
const val UNEXPECTED_ERROR = "Unexpected error"
val UNEXPECTED_ERROR_LISTED = arrayListOf("Unexpected error")
//const val PROBABLY_SERVER_ERROR = "Most probably server have sent to you some unexpected data"

// in internet connection error: no internet
//todo think about proper error message
const val INTERNET_CONNECTION_ERROR_CODE = 1
const val INTERNET_CONNECTION_ERROR =
    "Internet connection problem. Please, check you internet connection"

// in case of we don't have data or have some unexpected/invalid data
// happens on app side
const val INVALID_DATA_ERROR_CODE = 2
const val INVALID_DATA_ERROR = "Invalid data"

const val ERROR_CODE_UNAUTH = 401

/**
 * Class to hold response from server
 * can have 2 states
 * which could be created by methods [createFailedResponse] and [createSucceedResponse]
 *
 * contains fields:
 * [status] enum item to show is response succeed or not
 * [payload] contains response from server, can have any object there depending on response
 * [errorCode] int-value of response code
 * [errorMessages] messages contained error description. could be list of errors
 * (for example on response on profile creation: there could be errors for each field)
 */

class ResponseHolder<T> constructor(
    val headers: HashMap<String, List<String>>?,
    val status: ResponseStatus?,
    val payload: T?,
    val errorCode: Int?,
    val errorMessages: ArrayList<String>?
) {
    fun isSucceed(): Boolean {
        return status == ResponseStatus.SUCCEED
    }

    fun isInvalidDataError(): Boolean {
        return (status == ResponseStatus.FAILED) && (errorCode == INVALID_DATA_ERROR_CODE)
    }

    /**
     * checking is responseholder is with auth error
     */
    fun isAuthError(): Boolean {
        return (status == ResponseStatus.FAILED && errorCode == ERROR_CODE_UNAUTH)
    }

    companion object {
        /**
         * use in case of no need headers
         */
        fun <T> createSucceedResponse(
            payload: T?
        ): ResponseHolder<T> {
            return ResponseHolder(
                null,
                ResponseStatus.SUCCEED,
                payload,
                null,
                null
            )
        }

        /**
         * use in case of no need headers
         */
        fun <T> createFailedResponse(
            errorCode: Int?,
            errorMessages: ArrayList<String>?
        ): ResponseHolder<T> {
            return ResponseHolder(
                null,
                ResponseStatus.FAILED,
                null,
                errorCode,
                errorMessages
            )
        }

        fun <T> createSucceedResponse(
            payload: T?,
            headers: HashMap<String, List<String>>?
        ): ResponseHolder<T> {
            return ResponseHolder(
                headers,
                ResponseStatus.SUCCEED,
                payload,
                null,
                null
            )
        }

        fun <T> createFailedResponse(
            errorCode: Int?,
            errorMessages: ArrayList<String>?,
            headers: HashMap<String, List<String>>?
        ): ResponseHolder<T> {
            return ResponseHolder(
                headers,
                ResponseStatus.FAILED,
                null,
                errorCode,
                errorMessages
            )
        }

        fun <T> createUnexpectedError(): ResponseHolder<T> {
            return ResponseHolder(
                null,
                ResponseStatus.FAILED,
                null,
                UNEXPECTED_ERROR_CODE,
                arrayListOf(UNEXPECTED_ERROR)
            )
        }

        fun <T> createUnautorizeError(): ResponseHolder<T> {
            return ResponseHolder(
                null,
                ResponseStatus.FAILED,
                null,
                401,
                arrayListOf("Unauthorized")
            )
        }

        /**
         * use in case of some data is absent or invalid
         */
        fun <T> createInvalidDataError(): ResponseHolder<T> {
            return ResponseHolder(
                null,
                ResponseStatus.FAILED,
                null,
                INVALID_DATA_ERROR_CODE,
                arrayListOf(INVALID_DATA_ERROR)
            )
        }

        fun areAllResponsesSucceed(vararg responses: ResponseHolder<*>): Boolean {
            return areAllResponsesSucceed(arrayListOf(*responses))
        }

        fun areAllResponsesSucceed(responsesList: ArrayList<ResponseHolder<*>>): Boolean {
            for (responseHolder in responsesList) {
                if (!responseHolder.isSucceed()) {
                    return false
                }
            }

            return true
        }

        fun <T> getErrorResponseFromListOfResponses(vararg responses: ResponseHolder<*>): ResponseHolder<T?> {
            return getErrorResponseFromListOfResponses(arrayListOf(*responses))
        }

        fun <T> getErrorResponseFromListOfResponses(responsesList: List<ResponseHolder<*>>): ResponseHolder<T?> {
            val failedResponses = responsesList.filter { !it.isSucceed() }

            for (response in failedResponses) {
                if (response.errorCode != null && response.errorMessages != null) {
                    return createFailedResponse(
                        response.errorCode,
                        response.errorMessages,
                        response.headers
                    )
                }
            }

            return createUnexpectedError()
        }
    }

    override fun toString(): String {
        return "status: $status " +
                "payload: ${Gson().toJson(payload)} " +
                "error code: $errorCode " +
                "error message: ${Gson().toJson(errorMessages)} " +
                "headers: ${Gson().toJson(headers)} "
    }

    enum class ResponseStatus {
        SUCCEED, FAILED
    }
}