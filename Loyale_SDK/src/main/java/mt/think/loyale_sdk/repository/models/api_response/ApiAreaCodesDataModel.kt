package mt.think.loyale_sdk.repository.models.api_response

data class ApiAreaCodesDataModel(
    val code: String,
    val country: String,
    val iso: String
)