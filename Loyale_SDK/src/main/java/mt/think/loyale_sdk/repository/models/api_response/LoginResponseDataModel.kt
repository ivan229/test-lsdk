package mt.think.loyale_sdk.repository.models.api_response

open class LoginResponseDataModel(
    val token: String,
    val userId: String,
    val userName: String
)