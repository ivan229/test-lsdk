package mt.think.loyale_sdk.repository.models.api_response

data class ApiLevelDataModel(
    val id: String,
    val tierId: String,
    val schemeId: String,
    val name: String,
    val pointAllocationPerCurrency: String,
    val threshold: String,
    val pointExpiryPeriod: String,
    val pointRetentionPeriod: String,
    val guaranteedPeriod: String,
    val updatedDate: String,
    val createdDate: String
)