package mt.think.loyale_sdk.repository.models.api_body

data class ApiRequestEmailDataModel(
    val email: String
)