package mt.think.loyale_sdk.repository.models.api_response

data class ApiResponseFranchiseDataModel(
    val id: String,
    val name: String?,
    val description: String?,
    val schemeId: String,
    val imageUrl: String?,
    val imageGallery: ArrayList<String>?,
    val categories: ArrayList<String>?,
    val updatedDate: String,
    val createdDate: String,
    val hidden: Boolean
)