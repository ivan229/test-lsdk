package mt.think.loyale_sdk.repository

import android.app.Activity
import mt.think.loyale_sdk.repository.models.ResponseHolder
import mt.think.loyale_sdk.repository.models.api_response.ApiAreaCodesDataModel
import mt.think.loyale_sdk.repository.models.api_response.ApiCountryModel
import mt.think.loyale_sdk.repository.models.api_response.ApiCustomerResultView
import mt.think.loyale_sdk.repository.models.api_response.ApiTown
import mt.think.loyale_sdk.repository.network.ApiRequests
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import java.io.File

interface Repository/*(
    val activity: Activity,
    val baseUrl: String,
    val schemeId: String,
    //val dataCash: T
)*/ {

    val activity: Activity
    val baseUrl: String
    val schemeId: String

    //abstract val dataCash: T

    class Helpers(
        override val activity: Activity,
        override val baseUrl: String,
        override val schemeId: String
    ): Repository {
        /**
         * getting area's phone codes list
         */
        suspend fun getAreasCodes(): ResponseHolder<ArrayList<ApiAreaCodesDataModel>?> {
            return ApiRequests(baseUrl, activity).getAreasCodes()
        }

        suspend fun getCountriesList(): ResponseHolder<ArrayList<ApiCountryModel>?> {
            return ApiRequests(baseUrl, activity).getCountriesList()
        }

        suspend fun getTownsList(locale: String): ResponseHolder<ArrayList<ApiTown>?> {
            return ApiRequests(baseUrl, activity).getTownsByLocale(schemeId, locale)
        }

        suspend fun uploadProfileImage(customerId: String, token: String, file: File): ResponseHolder<Any?> {
            val requestFile = file
                .asRequestBody("image/png".toMediaTypeOrNull())
            val body = MultipartBody.Part.createFormData("file", "somename", requestFile)

            val description = "image/png".toRequestBody("multipart/form-data".toMediaTypeOrNull())

            return ApiRequests(baseUrl, activity).uploadProfileImage(
                schemeId,
                token,
                customerId,
                description,
                body,
            )
        }

        suspend fun getCustomer(
            customerId: String
        ): ResponseHolder<ApiCustomerResultView?> {
            return ApiRequests(baseUrl, activity).getCustomer(
                schemeId, customerId
            )
        }
    }

/*    *//**
     * getting area's phone codes list
     *//*
    suspend fun getAreasCodes(): ResponseHolder<ArrayList<ApiAreaCodesDataModel>?> {
        return ApiRequests(baseUrl, activity).getAreasCodes()
    }

    suspend fun getCountriesList(): ResponseHolder<ArrayList<ApiCountryModel>?> {
        return ApiRequests(baseUrl, activity).getCountriesList()
    }

    suspend fun getTownsList(locale: String): ResponseHolder<ArrayList<ApiTown>?> {
        return ApiRequests(baseUrl, activity).getTownsByLocale(schemeId, locale)
    }*/

//    /**
//     * performing login
//     */
//    open suspend fun performLogin(
//        email: String,
//        password: String
//    ): ResponseHolder<LoginResponseDataModel?> {
//        val responseHolder = ApiRequests(baseUrl, activity).login(schemeId, email, password)
//        if (responseHolder.isSucceed()) {
//            registerPushToken(responseHolder)
//            saveUserIdAndAuthToken(responseHolder.payload!!.userId, responseHolder.payload.token)
//            saveUserLoginData(SavedLoginDataModel(email, password))
//        }
//        return responseHolder
//    }
//
//    /**
//     * registering firebase push token to server
//     */
//    protected suspend fun registerPushToken(responseHolder: ResponseHolder<LoginResponseDataModel?>) {
//        try {
//            FirebaseInstanceId.getInstance().instanceId
//                .addOnCompleteListener(OnCompleteListener { task ->
//                    if (!task.isSuccessful) {
//                        return@OnCompleteListener
//                    }
//
//                    // Get new Instance ID token
//                    val token = task.result?.token
//                    saveNotificationToken(token ?: "")
//
//                    CoroutineScope(Dispatchers.Default).launch {
//                        ApiRequests(baseUrl, activity).registerPushToken(
//                            schemeId,
//                            token ?: "",
//                            responseHolder.payload!!.userId
//                        )
//                    }
//                })
//        } catch (e: Exception) {
//            Log.e("error: ", e.message!!)
//        }
//    }
//
//    /**
//     * saving locally user's ID and auth token
//     */
//    protected fun saveUserIdAndAuthToken(userId: String, token: String) {
//        dataCash.userToken = token
//        dataCash.userId = userId
//        LocalStorage(activity).saveUserIdAndAuthToken(
//            UserIdAuthTokenLocalStorageDataModel(
//                userId,
//                token
//            )
//        )
//    }
//
//    /**
//     * saving firebase notification token
//     */
//    private fun saveNotificationToken(notificationToken: String) {
//        dataCash.notificationToken = notificationToken
//        LocalStorage(activity).savedNotificationToken(notificationToken)
//    }
//
//    /**
//     * Saving user login and password to receive new auth tokens
//     */
//    protected fun saveUserLoginData(userLoginData: SavedLoginDataModel) {
//        LocalStorage(activity).saveUserLoginData(userLoginData)
//    }

}