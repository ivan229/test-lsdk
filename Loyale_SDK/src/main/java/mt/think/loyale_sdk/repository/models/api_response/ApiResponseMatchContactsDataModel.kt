package mt.think.loyale_sdk.repository.models.api_response

data class ApiResponseMatchContactsDataModel(
    val contactMatches: ArrayList<String>
)