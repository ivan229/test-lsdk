package mt.think.loyale_sdk.repository.models.api_response

data class ApiTown(
    val id: String,
    val name: String,
    val countryCode: String
)