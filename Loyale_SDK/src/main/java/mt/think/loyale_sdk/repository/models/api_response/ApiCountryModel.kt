package mt.think.loyale_sdk.repository.models.api_response

data class ApiCountryModel(
    val id: String,
    val name: String,
    val alpha2Code: String,
    val alpha3Code: String,
    val callingCode: String,
    val currencyName: String,
    val currencyCode: String,
    val currencySymbol: String,

    val languageNames: ArrayList<String>,
    val nativeNames: ArrayList<String>,
    val languageCodes: ArrayList<String>
)