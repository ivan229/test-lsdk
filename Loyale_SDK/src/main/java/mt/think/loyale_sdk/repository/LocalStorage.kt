package mt.think.loyale_sdk.repository

import android.app.Activity
import android.content.Context
import com.google.gson.Gson
import mt.think.loyale_sdk.repository.models.local_storage_models.SavedLoginDataModel
import mt.think.loyale_sdk.repository.models.local_storage_models.UserIdAuthTokenLocalStorageDataModel

const val SHARED_PREF = "sharedPreferenses"

const val KEY_NOTIFICATION_TOKEN = "keyNotificationToken"
const val KEY_AUTH_TOKEN_USER_ID = "keyAuthToken"
const val KEY_USER_LOGIN = "keyUserLoginPass"


open class LocalStorage(val activity: Activity) {

    /**
     * getting saved notification's token
     */
    fun getSavedNotificationToken(): String {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
        return sharedPref.getString(KEY_NOTIFICATION_TOKEN, "") ?: ""
    }


    /**
     * Saving notification's token
     */
    fun savedNotificationToken(notificationsToken: String) {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE) ?: return

        with(sharedPref.edit()) {
            putString(KEY_NOTIFICATION_TOKEN, notificationsToken)
            apply()
        }
    }

    /**
     * clearing all locally saved data
     * can be used for example when user logout
     */
    fun clearAllLocalData() {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            clear()
            apply()
        }
    }

    /**
     * saving auth token
     */
    fun saveUserIdAndAuthToken(userData: UserIdAuthTokenLocalStorageDataModel) {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(KEY_AUTH_TOKEN_USER_ID, Gson().toJson(userData))
            apply()
        }
    }

    /**
     * getting user auth token
     * or null in case if there is no saved token
     */
    fun getSavedTokenUserId(): UserIdAuthTokenLocalStorageDataModel? {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)

        val jsonString = sharedPref.getString(KEY_AUTH_TOKEN_USER_ID, null)
        return if (jsonString == null) {
            null
        } else {
            Gson().fromJson(jsonString, UserIdAuthTokenLocalStorageDataModel::class.java)
        }
    }

    /**
     * getting saved user login data
     * or null in case of there was no user saved
     */
    fun getSavedUser(): SavedLoginDataModel? {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE)
        val jsonLoginToPass = sharedPref.getString(KEY_USER_LOGIN, null)
        return if (jsonLoginToPass == null) {
            null
        } else {
            Gson().fromJson(jsonLoginToPass, SavedLoginDataModel::class.java)
        }
    }

    /**
     * locally saving login data
     */
    fun saveUserLoginData(loginData: SavedLoginDataModel) {
        val sharedPref = activity.getSharedPreferences(SHARED_PREF, Context.MODE_PRIVATE) ?: return
        with(sharedPref.edit()) {
            putString(KEY_USER_LOGIN, Gson().toJson(loginData))
            apply()
        }
    }

}