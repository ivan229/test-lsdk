package mt.think.loyale_sdk.repository.models.api_response

data class ApiResponsePhotoUpload(
    val url: String
)