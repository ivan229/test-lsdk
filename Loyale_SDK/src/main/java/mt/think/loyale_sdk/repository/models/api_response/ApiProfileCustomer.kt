package mt.think.loyale_sdk.repository.models.api_response

import mt.think.loyale_sdk.repository.models.api_inner_models.ApiDetailsLevel

data class ApiProfileCustomer(
    val id: String,
    val profileImageUrl: String?,
    val firstName: String,
    val lastName: String,
    val dob: String?,
    val email: String,
    val gender: Int?,
    val areaCode: String,
    val mobileNumber: String,
    val addressLine1: String?,
    val addressLine2: String?,
    val town: String?,
    val state: String?,
    val postCode: String?,
    val country: String?,
    val marketingSub: Boolean,

    val detailedLevels: ArrayList<ApiDetailsLevel>?,

    val mobileNumberVerified: Boolean
)