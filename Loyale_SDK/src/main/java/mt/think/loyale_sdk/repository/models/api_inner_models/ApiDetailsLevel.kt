package mt.think.loyale_sdk.repository.models.api_inner_models

data class ApiDetailsLevel(
    val levelId: String,
    val tierId: String,
    val schemeId: String,
    val createdDate: String,
    val updatedDate: String
)