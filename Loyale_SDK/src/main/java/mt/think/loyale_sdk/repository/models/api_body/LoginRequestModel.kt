package mt.think.loyale_sdk.repository.models.api_body

open class LoginRequestModel(val email: String, val password: String)