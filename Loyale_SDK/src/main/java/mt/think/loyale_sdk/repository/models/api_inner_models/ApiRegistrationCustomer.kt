package mt.think.loyale_sdk.repository.models.api_inner_models

data class ApiRegistrationCustomer(
    val areaCode: String,
    val dob: String?,
    val email: String,
    val firstName: String,
    val gender: Int?,
    val lastName: String,
    val marketingSub: Boolean,
    val mobileNumber: String,
    val addressLine1: String?,
    val addressLine2: String?,
    val town: String?,
    val state: String?,
    val postCode: String?,
    val country: String?,
    val password: String,
    val referralCode: String?,
    val signUpPlatform: String? //json string formated as ApiSignUpPlatformModel class
)