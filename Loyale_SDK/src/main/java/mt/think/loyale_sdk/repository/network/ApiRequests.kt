package mt.think.loyale_sdk.repository.network

import android.content.Context
import mt.think.loyale_sdk.repository.models.ResponseHolder
import mt.think.loyale_sdk.repository.models.api_body.*
import mt.think.loyale_sdk.repository.models.api_response.*
import mt.think.loyale_sdk.repository.network.network_utils.RetrofitCreator
import mt.think.loyale_sdk.repository.network.network_utils.performWithErrorHandling
import okhttp3.MultipartBody
import okhttp3.RequestBody

const val BEARER = "Bearer "

open class ApiRequests(val baseUrl: String, val applicationContext: Context) {

    /**
     * getting phone codes of countries
     */
    suspend fun getAreasCodes(): ResponseHolder<ArrayList<ApiAreaCodesDataModel>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl).getAreasCodes()
        }
    }

    suspend fun getCountriesList(): ResponseHolder<ArrayList<ApiCountryModel>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl).getCountriesList()
        }
    }

    suspend fun getTownsByLocale(
        schemeId: String,
        locale: String
    ): ResponseHolder<ArrayList<ApiTown>?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getTownsByLocale(schemeId, locale)
        }
    }

    suspend fun uploadProfileImage(
        schemeId: String,
        token: String,
        customerId: String,
        description: RequestBody,
        file: MultipartBody.Part
    ): ResponseHolder<Any?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl).uploadProfilePicture(
                schemeId,
                token,
                customerId,
                description,
                file
            )
        }
    }

    suspend fun getCustomer(
        schemeId: String,
        customerId: String
    ): ResponseHolder<ApiCustomerResultView?> {
        return performWithErrorHandling(applicationContext) {
            RetrofitCreator.getRetrofit(baseUrl)
                .getCustomer(schemeId, customerId)
        }
    }

//    suspend fun login(
//        schemeId: String,
//        login: String,
//        pass: String
//    ): ResponseHolder<LoginResponseDataModel?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .authUser(schemeId, LoginRequestModel(login, pass))
//        }
//    }
//
//    suspend fun registerPushToken(
//        schemeId: String,
//        token: String,
//        userId: String
//    ): ResponseHolder<Any?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .registerPushToken(
//                    schemeId,
//                    ApiRequestRegistedPushToken(userId, token)
//                )
//        }
//    }
//
//    suspend fun unsubscribeNotifications(
//        schemeId: String,
//        token: String,
//        notificationToken: String
//    ): ResponseHolder<Any?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .unsubscribeNotifications(
//                    schemeId, BEARER + token, notificationToken
//                )
//        }
//    }
//
//    suspend fun getFranchaise(
//        schemeId: String,
//        token: String,
//        filter: String?
//    ): ResponseHolder<ArrayList<ApiResponseFranchiseDataModel>?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .getFranchise(schemeId, BEARER + token, filter)
//        }
//    }
//
//    suspend fun register(
//        registrationDataModel: ApiRegistrationDataModel,
//        schemeId: String
//    ): ResponseHolder<ApiRegistrationDataModel?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .registerNewUser(schemeId, registrationDataModel)
//        }
//    }
//
//    /**
//     * request to send email with link to restore pass to specified email
//     * in case of succeed expect some string like "Email for resetting password was sent to: email2@new.com"
//     */
//    suspend fun requestRestoringPass(
//        schemeId: String,
//        email: String
//    ): ResponseHolder<String?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .requestRestoringPass(schemeId, ApiRequestEmailDataModel(email))
//        }
//    }
//
//    suspend fun changePassword(
//        schemeId: String,
//        changePassDataModel: ApiRequestChangePassDataModel,
//        token: String
//    )
//            : ResponseHolder<Any?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .changePassword(schemeId, BEARER + token, changePassDataModel)
//        }
//    }
//
//    suspend fun uploadPhoto(
//        token: String,
//        description: RequestBody,
//        file: MultipartBody.Part,
//        schemeId: String
//    ): ResponseHolder<ApiResponsePhotoUpload?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl).uploadPhoto(
//                schemeId,
//                BEARER + token,
//                description,
//                file
//            )
//        }
//    }
//
//    suspend fun matchContacts(
//        schemeId: String,
//        token: String,
//        phoneNumbers: ArrayList<String>
//    ): ResponseHolder<ApiResponseMatchContactsDataModel?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .matchContacts(
//                    schemeId, BEARER + token, ApiRequestMatchContactsDataModel(phoneNumbers)
//                )
//        }
//    }
//
//    suspend fun sendMessage(
//        token: String,
//        schemeId: String,
//        message: ApiRequestMessage
//    ): ResponseHolder<Any?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .sendMessage(schemeId = schemeId, authHeader = BEARER + token, message = message)
//        }
//    }
//
//    suspend fun getCurrencyPointConversionOverride(
//        schemeId: String,
//        token: String,
//        filter: String
//    ): ResponseHolder<ArrayList<ApiResponseCoversionRate>?> {
//        return performWithErrorHandling(applicationContext) {
//            RetrofitCreator.getRetrofit(baseUrl)
//                .getCurrencyPointConversionOverride(schemeId, BEARER + token, filter)
//        }
//    }

}