package mt.think.loyale_sdk.repository.models.api_body

import mt.think.loyale_sdk.repository.models.api_inner_models.ApiRegistrationAdditionalField
import mt.think.loyale_sdk.repository.models.api_inner_models.ApiRegistrationCustomer

data class ApiRegistrationDataModel(
    val additionalFields: List<ApiRegistrationAdditionalField>,
    val customer: ApiRegistrationCustomer
)