package mt.think.loyale_sdk.repository.models.api_body

data class ApiRequestRegistedPushToken(
    val customerId: String,
    val token: String
)