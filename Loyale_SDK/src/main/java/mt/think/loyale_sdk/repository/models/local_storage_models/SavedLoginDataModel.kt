package mt.think.loyale_sdk.repository.models.local_storage_models

open class SavedLoginDataModel(val email: String, val password: String)