package mt.think.loyale_sdk.repository.models.api_body

data class ApiRequestMatchContactsDataModel(
    val contacts: ArrayList<String>
)