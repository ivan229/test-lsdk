package mt.think.loyale_sdk.repository.models.api_response

data class ApiResponseCoversionRate(
    val id: String,
    val overridingId: String,
    val levelId: String,
    val pointAllocationPerCurrency: Int,
    val createdDate: String,
    val createdBy: String,
    val updatedDate: String,
    val updatedBy: String
)