package mt.think.loyale_sdk.repository

abstract class DataCash {
    //user id
    var userId: String? = null

    //auth token
    var userToken: String? = null

    //additional profile fields data
    var additionalFieldKeyToIdMap: HashMap<String, String>? = null

    // notification token to subscribe/unsubscribe from notifications
    var notificationToken: String? = null


    open fun clearFields() {
        userToken = null
        userId = null
        additionalFieldKeyToIdMap = null
    }
}