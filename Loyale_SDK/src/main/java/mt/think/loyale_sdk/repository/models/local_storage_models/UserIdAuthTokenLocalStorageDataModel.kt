package mt.think.loyale_sdk.repository.models.local_storage_models

data class UserIdAuthTokenLocalStorageDataModel(
    val userId: String,
    val authToken: String
)